#Description
This is my take on an evolution of the classic game of chess. Some features have yet to be implemented so if something isn't working how you think it should, check the ToDo section of this Readme. The rules are subject to change.

#Controls
Currently, WASD are the only keys to move with and the mouse looks around. Left click will select a piece and the space to move that piece. Right click will deselect the piece. Alt-F4 is the only way to quit.

#Rules
* The normal rules of chess apply except for the following:
	* The king does not have to be put in checkmate. It just has to be captured like any other piece.
	* Castling and en passant have not been implemented yet.
	* Pawn promotion automatically selects the queen. This can be changed if requested in the future.
* Two new pieces have been added to the game and their rules are as follows:
	* Prince
		* Moves 2-3 spaces in any direction, hopping over other pieces similar to the Knight.
		* Starts to the right of the King.
	* Guard
		* Moves one space in any direction.
		* Cannot capture enemy pieces.
		* Cannot be captured unless at least two other enemy pieces are adjacent to it. Diagonal does not count as adjacent.
* There are five green spaces on the board. These are objectives. A player claims an objective by having a piece on that space.
* Controlling three or more of the objectives at the end of a turn gives that player a point. The player wins by having five points or by capturing the king.

#ToDo
* Pause Menu
	* Quit the game
	* Edit controls
* Castling
* En Passant
* Creating models for the new pieces
* Better main menu
* Showing the winner at the end

#Playing
In the builds folder, there are built versions for Windows, macOS, and Linux. The Mac and Windows versions both work, the Linux version has yet to be tested.
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Prince : Piece {

	public override List<Vector2Int> MoveLocations(int x, int z) {
		List<Vector2Int> locations = new List<Vector2Int>();
		locations.Add(new Vector2Int(x + 2, z));
		locations.Add(new Vector2Int(x + 2, z + 1));
		locations.Add(new Vector2Int(x + 2, z - 1));
		locations.Add(new Vector2Int(x + 3, z));
		locations.Add(new Vector2Int(x - 2, z));
		locations.Add(new Vector2Int(x - 2, z + 1));
		locations.Add(new Vector2Int(x - 2, z - 1));
		locations.Add(new Vector2Int(x - 3, z));
		locations.Add(new Vector2Int(x, z + 2));
		locations.Add(new Vector2Int(x + 1, z + 2));
		locations.Add(new Vector2Int(x - 1, z + 2));
		locations.Add(new Vector2Int(x, z + 3));
		locations.Add(new Vector2Int(x, z - 2));
		locations.Add(new Vector2Int(x + 1, z - 2));
		locations.Add(new Vector2Int(x - 1, z - 2));
		locations.Add(new Vector2Int(x, z - 3));
		return locations;
	}
	
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rook : Piece {

	public bool hasMoved = false;
	public override List<Vector2Int> MoveLocations(int x, int z) {
		List<Vector2Int> locations = new List<Vector2Int>();
		foreach (Vector2Int dir in RookDirections) {
			for (int i = 1; i < 15; i++) {
				Vector2Int nextGridPoint = new Vector2Int(x + i * dir.x, z + i * dir.y);
				locations.Add(nextGridPoint);
				if (GameManager.instance.PieceAtGrid(nextGridPoint.x, nextGridPoint.y)) {
					break;
				}
			}
		}
		return locations;
	}

}

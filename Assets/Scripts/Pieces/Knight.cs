﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Knight : Piece {

	public override List<Vector2Int> MoveLocations(int x, int z) {
		List<Vector2Int> locations = new List<Vector2Int>();
		locations.Add(new Vector2Int(x + 2, z + 1));
		locations.Add(new Vector2Int(x + 2, z - 1));
		locations.Add(new Vector2Int(x - 2, z + 1));
		locations.Add(new Vector2Int(x - 2, z - 1));
		locations.Add(new Vector2Int(x + 1, z + 2));
		locations.Add(new Vector2Int(x + 1, z - 2));
		locations.Add(new Vector2Int(x - 1, z + 2));
		locations.Add(new Vector2Int(x - 1, z - 2));
		return locations;
	}
}

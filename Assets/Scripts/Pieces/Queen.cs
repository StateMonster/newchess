﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Queen : Piece {

	public override List<Vector2Int> MoveLocations(int x, int z) {
		List<Vector2Int> locations = new List<Vector2Int>();
		List<Vector2Int> directions = new List<Vector2Int>(BishopDirections);
		directions.AddRange(RookDirections);
		foreach (Vector2Int dir in directions) {
			for (int i = 1; i < 15; i++) {
				Vector2Int nextGridPoint = new Vector2Int(x + i * dir.x, z + i * dir.y);
				locations.Add(nextGridPoint);
				if (GameManager.instance.PieceAtGrid(nextGridPoint.x, nextGridPoint.y)) {
					break;
				}
			}
		}
		return locations;
	}
	
}

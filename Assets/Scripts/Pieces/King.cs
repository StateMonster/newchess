﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class King : Piece {

	public bool hasMoved = false;
	public override List<Vector2Int> MoveLocations(int x, int z) {
		List<Vector2Int> locations = new List<Vector2Int>();
		List<Vector2Int> directions = new List<Vector2Int>(BishopDirections);
		directions.AddRange(RookDirections);
		foreach (Vector2Int dir in directions) {
			Vector2Int nextGridPoint = new Vector2Int(x + 1 * dir.x, z + 1 * dir.y);
			locations.Add(nextGridPoint);
		}
		return locations;
	}
}

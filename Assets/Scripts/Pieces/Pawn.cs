﻿/*
 * Copyright (c) 2018 Razeware LLC
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * Notwithstanding the foregoing, you may not use, copy, modify, merge, publish, 
 * distribute, sublicense, create a derivative work, and/or sell copies of the 
 * Software in any work that is designed, intended, or marketed for pedagogical or 
 * instructional purposes related to programming, coding, application development, 
 * or information technology.  Permission for such use, copying, modification,
 * merger, publication, distribution, sublicensing, creation of derivative works, 
 * or sale is expressly withheld.
 *    
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

using System.Collections.Generic;
using UnityEngine;

public class Pawn : Piece
{

	public bool doubleMove = true;
    public override List<Vector2Int> MoveLocations(int x, int z)
    {
        List<Vector2Int> locations = new List<Vector2Int>();
		int forwardDirection = GameManager.instance.currentPlayer.forward;
		Vector2Int forward = new Vector2Int(x, z + forwardDirection);
		if (GameManager.instance.PieceAtGrid(forward.x, forward.y) == false) {
			locations.Add(forward);
		}
		Vector2Int forwardRight = new Vector2Int(x + 1, z + forwardDirection);
		if (GameManager.instance.PieceAtGrid(forwardRight.x, forwardRight.y)) {
			locations.Add(forwardRight);
		}
		Vector2Int forwardLeft = new Vector2Int(x - 1, z + forwardDirection);
		if (GameManager.instance.PieceAtGrid(forwardLeft.x, forwardLeft.y)) {
			locations.Add(forwardLeft);
		}
		if (doubleMove) {
			Vector2Int doubleForward = new Vector2Int(x, z + (2*forwardDirection));
			if (GameManager.instance.PieceAtGrid(doubleForward.x, doubleForward.y) == null) {
				locations.Add(doubleForward);
			}
		}
        return locations;
    }
}

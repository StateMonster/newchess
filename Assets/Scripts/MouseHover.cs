﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseHover : MonoBehaviour {

	// Use this for initialization
	void Start () {
		this.GetComponent<Renderer>().material.color = Color.black;
	}
	
	void onMouseEnter() {
		this.GetComponent<Renderer>().material.color = Color.red;
	}

	void onMouseExit() {
		this.GetComponent<Renderer>().material.color = Color.black;
	}

}

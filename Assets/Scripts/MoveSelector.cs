﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveSelector : MonoBehaviour {

	public GameObject moveLocationPrefab;
	public GameObject tileHighlightPrefab;
	public GameObject attackLocationPrefab;
	private GameObject tileHighlight;
	private GameObject movingPiece;
	private List<Vector2Int> moveLocations;
	private List<GameObject> locationHighlights;
	private bool moved;

	// Use this for initialization
	void Start () {
		this.enabled = false;
		moved = false;
		tileHighlight = Instantiate(tileHighlightPrefab, new Vector3(0, 0, 0), Quaternion.identity, gameObject.transform);
		tileHighlight.SetActive(false);
	}
	
	// Update is called once per frame
	void Update () {
		Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
		RaycastHit hit;
		if (Physics.Raycast(ray, out hit)) {
			GameObject space = hit.transform.gameObject;
			tileHighlight.SetActive(true);
			tileHighlight.transform.position = new Vector3(space.transform.position.x, space.transform.position.y + 0.01f, space.transform.position.z);
			if (Input.GetMouseButtonDown(0)) {
				Vector2Int move = new Vector2Int((int)space.transform.position.x, (int)space.transform.position.z);
				if (!moveLocations.Contains(move)) {
					return;
				}
				if (GameManager.instance.PieceAtGrid(move.x, move.y) == null) {
					GameManager.instance.Move(movingPiece, move.x, move.y);
					moved = true;
				} else {
					if (GameManager.instance.CapturePieceAt(move.x, move.y, movingPiece)) {
						GameManager.instance.Move(movingPiece, move.x, move.y);
						moved = true;
					}
				}
				ExitState();
			} else if (Input.GetMouseButtonDown(1)) {
				ExitState();
			}
		} else {
			tileHighlight.SetActive(false);
		}
	}

	public void EnterState(GameObject piece) {
		movingPiece = piece;
		this.enabled = true;
		moveLocations = GameManager.instance.MovesForPiece(movingPiece);
		locationHighlights = new List<GameObject>();
		foreach (Vector2Int loc in moveLocations) {
			GameObject highlight;
			if (GameManager.instance.PieceAtGrid(loc.x, loc.y)) {
				highlight = Instantiate(attackLocationPrefab, new Vector3(loc.x, 0.01f, loc.y), Quaternion.identity, gameObject.transform);
			} else {
				highlight = Instantiate(moveLocationPrefab, new Vector3(loc.x, 0.01f, loc.y), Quaternion.identity, gameObject.transform);
			}
			locationHighlights.Add(highlight);
		}
	}

	public void ExitState() {
		this.enabled = false;
		tileHighlight.SetActive(false);
		GameManager.instance.DeselectPiece(movingPiece);
		movingPiece = null;
		TileSelector selector = GetComponent<TileSelector>();
		if (moved) {
			GameManager.instance.NextPlayer();
			moved = false;
		}
		selector.EnterState();
		foreach (GameObject highlight in locationHighlights) {
			Destroy(highlight);
		}
	}
}

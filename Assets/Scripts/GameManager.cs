﻿/*
 * Copyright (c) 2018 Razeware LLC
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * Notwithstanding the foregoing, you may not use, copy, modify, merge, publish, 
 * distribute, sublicense, create a derivative work, and/or sell copies of the 
 * Software in any work that is designed, intended, or marketed for pedagogical or 
 * instructional purposes related to programming, coding, application development, 
 * or information technology.  Permission for such use, copying, modification,
 * merger, publication, distribution, sublicensing, creation of derivative works, 
 * or sale is expressly withheld.
 *    
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
	public int width;
	public int height;
	private Material defaultMaterial;
    public Material selectedMaterial;
    public static GameManager instance;
	public GameObject whiteSpace;
	public GameObject blackSpace;
	public GameObject objSpace;
    public GameObject whiteKing;
    public GameObject whiteQueen;
    public GameObject whiteBishop;
    public GameObject whiteKnight;
    public GameObject whiteRook;
    public GameObject whitePawn;
    public GameObject whiteGuard;
    public GameObject whitePrince;
    public GameObject blackKing;
    public GameObject blackQueen;
    public GameObject blackBishop;
    public GameObject blackKnight;
    public GameObject blackRook;
    public GameObject blackPawn;
    public GameObject blackGuard;
    public GameObject blackPrince;
    public GameObject Player1Camera;
    public GameObject Player2Camera;

    private GameObject[,] pieces;

    private Player white;
    private Player black;
    public Player currentPlayer;
    public Player otherPlayer;

    void Awake()
    {
        instance = this;
    }

    void Start ()
    {
        pieces = new GameObject[15, 15];
        white = new Player("white", true);
        black = new Player("black", false);
        currentPlayer = white;
        otherPlayer = black;
        Player2Camera.SetActive(false);
        InitialSetup();
    }

    private void InitialSetup()
    {
		CreateBoard();
        AddPiece(whiteRook, white, 2, 0);
        AddPiece(whiteKnight, white, 3, 0);
        AddPiece(whiteBishop, white, 4, 0);
        AddPiece(whiteGuard, white, 5, 0);
        AddPiece(whiteQueen, white, 6, 0);
        AddPiece(whiteKing, white, 7, 0);
        AddPiece(whitePrince, white, 8, 0);
        AddPiece(whiteGuard, white, 9, 0);
        AddPiece(whiteBishop, white, 10, 0);
        AddPiece(whiteKnight, white, 11, 0);
        AddPiece(whiteRook, white, 12, 0);

        for (int i = 2; i < 13; i++)
        {
            AddPiece(whitePawn, white, i, 1);
        }

        AddPiece(blackRook, black, 2, 14);
        AddPiece(blackKnight, black, 3, 14);
        AddPiece(blackBishop, black, 4, 14);
        AddPiece(blackGuard, black, 5, 14);
        AddPiece(blackQueen, black, 6, 14);
        AddPiece(blackKing, black, 7, 14);
        AddPiece(blackPrince, black, 8, 14);
        AddPiece(blackGuard, black, 9, 14);
        AddPiece(blackBishop, black, 10, 14);
        AddPiece(blackKnight, black, 11, 14);
        AddPiece(blackRook, black, 12, 14);

        for (int i = 2; i < 13; i++)
        {
            AddPiece(blackPawn, black, i, 13);
        }
    }

	public void CreateBoard() {
		if (width % 2 == 0) {
			width++;
		}
		if (height % 2 == 0) {
			height++;
		}
		for (int x = 0; x < width; x++) {
			for (int z = 0; z < height; z++) {
				if ((x == 3 && (z == 4 || z == 10)) || (x == 11 && (z == 4 || z == 10)) || (x == 7 && z == 7)) {
					Instantiate(objSpace, new Vector3(x, 0, z), Quaternion.identity);
				} else if (x == 0 || x % 2 == 0) {
					if (z == 0 || z % 2 == 0) {
						Instantiate(blackSpace, new Vector3(x, 0, z), Quaternion.identity);
					} else {
						Instantiate(whiteSpace, new Vector3(x, 0, z), Quaternion.identity);
					}
				} else {
					if (z == 0 || z % 2 == 0) {
						Instantiate(whiteSpace, new Vector3(x, 0, z), Quaternion.identity);
					} else {
						Instantiate(blackSpace, new Vector3(x, 0, z), Quaternion.identity);
					}
				}
			}
		}
	}

    public void AddPiece(GameObject prefab, Player player, int col, int row)
    {
        GameObject pieceObject = Instantiate(prefab, new Vector3(col, 0, row),
            Quaternion.identity, gameObject.transform);
        player.pieces.Add(pieceObject);
        pieces[col, row] = pieceObject;
    }

    public void SelectPieceAtGrid(Vector2Int gridPoint)
    {
        GameObject selectedPiece = pieces[gridPoint.x, gridPoint.y];
        if (selectedPiece)
        {
			MeshRenderer renderers = selectedPiece.GetComponentInChildren<MeshRenderer>();
        	renderers.material = selectedMaterial;
        }
    }

    public void SelectPiece(GameObject piece)
    {
        MeshRenderer renderers = piece.GetComponentInChildren<MeshRenderer>();
		defaultMaterial = renderers.material;
        renderers.material = selectedMaterial;
    }

    public void DeselectPiece(GameObject piece)
    {
        MeshRenderer renderers = piece.GetComponentInChildren<MeshRenderer>();
        renderers.material = defaultMaterial;
    }

    public GameObject PieceAtGrid(int x, int z)
    {
        if (x > 14 || z > 14 || x < 0 || z < 0)
        {
            return null;
        }
        return pieces[x, z];
    }

    public Vector2Int GridForPiece(GameObject piece)
    {
        for (int i = 0; i < 15; i++) 
        {
            for (int j = 0; j < 15; j++)
            {
                if (pieces[i, j] == piece)
                {
                    return new Vector2Int(i, j);
                }
            }
        }

        return new Vector2Int(-1, -1);
    }

    public bool FriendlyPieceAt(Vector2Int gridPoint)
    {
        GameObject piece = PieceAtGrid(gridPoint.x, gridPoint.y);

        if (piece == null) {
            return false;
        }

        if (otherPlayer.pieces.Contains(piece))
        {
            return false;
        }

        return true;
    }

    public bool DoesPieceBelongToCurrentPlayer(GameObject piece)
    {
        return currentPlayer.pieces.Contains(piece);
    }
    public void Move(GameObject piece, int x, int z)
    {
        Vector2Int startGridPoint = GridForPiece(piece);
        pieces[startGridPoint.x, startGridPoint.y] = null;
        pieces[x, z] = piece;
        piece.transform.position = new Vector3(x, 0, z);
        if (piece.GetComponent<Pawn>() != null) {
            piece.GetComponent<Pawn>().doubleMove = false;
        }
        if (piece.GetComponent<King>() != null) {
            piece.GetComponent<King>().hasMoved = true;
        }
        if (piece.GetComponent<Rook>() != null) {
            piece.GetComponent<Rook>().hasMoved = true;
        }
        if ((currentPlayer.name == "white" && piece.transform.position.z == 14) || (currentPlayer.name == "black" && piece.transform.position.z == 0)) {
            otherPlayer.capturedPieces.Add(piece);
            Destroy(piece);
            if (currentPlayer.name == "white") {
                AddPiece(whiteQueen, white, x, z);
            } else {
                AddPiece(blackQueen, black, x, z);
            }
        }
        updateScore();
    }

    public void updateScore() {
        int x = 3;
        int z = 4;
        int currentPlayerScore = 0;
        int otherPlayerScore = 0;
        Vector2Int check;
        if (PieceAtGrid(x, z) != null) {
            check = new Vector2Int(x, z);
            if (FriendlyPieceAt(check)) {
                currentPlayerScore++;
            } else {
                otherPlayerScore++;
            }
        }
        z = 10;
        if (PieceAtGrid(x, z) != null) {
            check = new Vector2Int(x, z);
            if (FriendlyPieceAt(check)) {
                currentPlayerScore++;
            } else {
                otherPlayerScore++;
            }
        }
        x = 11;
        if (PieceAtGrid(x, z) != null) {
            check = new Vector2Int(x, z);
            if (FriendlyPieceAt(check)) {
                currentPlayerScore++;
            } else {
                otherPlayerScore++;
            }
        }
        z = 4;
        if (PieceAtGrid(x, z) != null) {
            check = new Vector2Int(x, z);
            if (FriendlyPieceAt(check)) {
                currentPlayerScore++;
            } else {
                otherPlayerScore++;
            }
        }
        x = 7;
        z = 7;
        if (PieceAtGrid(x, z) != null) {
            check = new Vector2Int(x, z);
            if (FriendlyPieceAt(check)) {
                currentPlayerScore++;
            } else {
                otherPlayerScore++;
            }
        }
        currentPlayer.score = currentPlayerScore;
        otherPlayer.score = otherPlayerScore;
        if (currentPlayer.score == 3) {
            Debug.Log(currentPlayer.name + " wins!");
            Destroy(this.GetComponent<TileSelector>());
			Destroy(this.GetComponent<MoveSelector>());
        }
    }

	public List<Vector2Int> MovesForPiece(GameObject pieceObject) {
		Piece piece = pieceObject.GetComponent<Piece>();
		Vector2Int gridPoint = GridForPiece(pieceObject);
		List<Vector2Int> locations = piece.MoveLocations(gridPoint.x, gridPoint.y);
		locations.RemoveAll(gp => gp.x < 0 || gp.x > 14 || gp.y < 0 || gp.y > 14);
		locations.RemoveAll(gp => FriendlyPieceAt(gp));
        /*Piece rightRook = PieceAtGrid(gridPoint.x, gridPoint.y + 5).GetComponent<Rook>();
        Piece leftRook = PieceAtGrid(gridPoint.x, gridPoint.y - 5).GetComponent<Rook>();
        if (pieceObject.GetComponent<King>() != null && pieceObject.GetComponent<King>().hasMoved == false && rightRook != null && rightRook.hasMoved == false) {
            locations.Add(new Vector2Int(gridPoint.x, gridPoint.y + 3));
        }
        if (pieceObject.GetComponent<King>() != null && pieceObject.GetComponent<King>().hasMoved == false && leftRook != null && leftRook.hasMoved == false) {
            locations.Add(new Vector2Int(gridPoint.x, gridPoint.y - 3));
        }
        */
		return locations;
	}

	public void RemovePiece(GameObject piece)
    {
        Destroy(piece);
    }

	public void NextPlayer() {
		Player tempPlayer = currentPlayer;
		currentPlayer = otherPlayer;
		otherPlayer = tempPlayer;
        if (Player1Camera.activeSelf) {
            Player1Camera.SetActive(false);
            Player2Camera.transform.position = new Vector3(7, 1.5f, 18);
            Player2Camera.SetActive(true);
        } else {
            Player2Camera.SetActive(false);
            Player1Camera.transform.position = new Vector3(7, 1.5f, -4);
            Player1Camera.SetActive(true);
        }
	}

    public bool checkNextTo(int x, int z, GameObject movingPiece) {
        if (PieceAtGrid(x, z) != null && DoesPieceBelongToCurrentPlayer(PieceAtGrid(x, z)) && PieceAtGrid(x, z) != movingPiece) {
                return true;
        }
        return false;
    }

	public bool CapturePieceAt(int x, int z, GameObject movingPiece) {
		GameObject pieceToCapture = PieceAtGrid(x, z);
        if (pieceToCapture.GetComponent<Guard>() != null) {
            int nextTo = 0;
            if (checkNextTo(x + 1, z, movingPiece)) {
                nextTo++;
            }
            if (checkNextTo(x - 1, z, movingPiece)) {
                nextTo++;
            }
            if (checkNextTo(x, z + 1, movingPiece)) {
                nextTo++;
            }
            if (checkNextTo(x, z - 1, movingPiece)) {
                nextTo++;
            }
            if (nextTo >= 2) {
                currentPlayer.capturedPieces.Add(pieceToCapture);
                pieces[x, z] = null;
                if (pieceToCapture.GetComponent<Piece>().type == PieceType.King) {
                    Debug.Log(currentPlayer.name + " wins!");
                    Destroy(this.GetComponent<TileSelector>());
                    Destroy(this.GetComponent<MoveSelector>());
                }
                Destroy(pieceToCapture);
                return true;
            }
            return false;
        } else {
            currentPlayer.capturedPieces.Add(pieceToCapture);
            pieces[x, z] = null;
            if (pieceToCapture.GetComponent<Piece>().type == PieceType.King) {
                Debug.Log(currentPlayer.name + " wins!");
                Destroy(this.GetComponent<TileSelector>());
                Destroy(this.GetComponent<MoveSelector>());
            }
            Destroy(pieceToCapture);
            return true;
        }
	}
}

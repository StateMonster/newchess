﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TileSelector : MonoBehaviour {

	public GameObject tileHightlightPrefab;
	private GameObject tileHighlight;

	// Use this for initialization
	void Start () {
		Vector2Int gridPoint = Geometry.GridPoint(0, 0);
		Vector3 point = Geometry.PointFromGrid(gridPoint);
		tileHighlight = Instantiate(tileHightlightPrefab, point, Quaternion.identity, gameObject.transform);
		tileHighlight.SetActive(false);
	}

	public void EnterState() {
		enabled = true;
	}
	
	// Update is called once per frame
	void Update () {
		Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
		RaycastHit hit;
		if (Physics.Raycast(ray, out hit)) {
			GameObject space = hit.transform.gameObject;
			tileHighlight.SetActive(true);
			tileHighlight.transform.position = new Vector3(space.transform.position.x, space.transform.position.y + 0.01f, space.transform.position.z);
			if (Input.GetMouseButtonDown(0)) {
				GameObject selectedPiece = GameManager.instance.PieceAtGrid((int)space.transform.position.x, (int)space.transform.position.z);
				if (GameManager.instance.DoesPieceBelongToCurrentPlayer(selectedPiece)) {
					GameManager.instance.SelectPiece(selectedPiece);
					ExitState(selectedPiece);
				}
			}
		} else {
			tileHighlight.SetActive(false);
		}
	}

	private void ExitState(GameObject movingPiece) {
		this.enabled = false;
		tileHighlight.SetActive(false);
		MoveSelector move = GetComponent<MoveSelector>();
		move.EnterState(movingPiece);
	}
}
